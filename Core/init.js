var server = require('./loader');
var avocado = new server();
var cluster = require('cluster');
var numCPUs = require('os').cpus().length

avocado.connect(function (err , db){
	avocado.initDatabase();
	avocado.initAutoLoad();
	avocado.initSession();
	// use handler
	avocado.initAvocado();
	if (cluster.isMaster) {
		console.log(`Master ${process.pid} is running`)

		for (let i = 0; i < numCPUs; i++) {
			console.log(`Forking process number ${i}...`)
			cluster.fork()
		}
	} else {
		avocado.run();
	}
});
// console.log(err);
// if (err) {
// 	// use handler
// 	avocado.initDatabase();
// 	avocado.initAvocado();
// 	avocado.run();
// } else {
// }