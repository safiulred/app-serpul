var express = require('express'),
	app = express(),
	mode = process.env.NODE_ENV || 'dev',
	bodyParser = require('body-parser'),
	xmlparser = require('express-xml-bodyparser'),
	path = require('path'),
	cookieParser = require('cookie-parser'),
	routes = require('../App/routes');

var autoload = require('../App/config/autoload.json');
autoload = autoload[mode] || autoload['dev'];
var fs = require('fs');
var config_database;
var setup = false;
var status_db = false;
var err_db = '';

var session = require('express-session');
var config_session = require('../App/config/session.json');
config_session = config_session[mode] || config_session['dev'];
var configApp = require('../App/config/app.json')[mode] || require('../App/config/app.json')['dev'];

function Avocado() {	
	this.app = app;
	this.db = null;

	this.run = function () {
		var configApp = require('../App/config/app.json')[mode] || require('../App/config/app.json')['dev'];
		configApp.app.port = process.env.OPENSHIFT_NODEJS_PORT || configApp.app.port;
		configApp.app.host = process.env.OPENSHIFT_NODEJS_IP || configApp.app.host;
		app.listen(configApp.app.port, configApp.app.host , function() {
			console.log("Application Running On " + configApp.app.host + ":" + configApp.app.port);
		});
	}

	this.connect = function (cb) {
		if (fs.existsSync('./App/config/database.json')) {
			config_database = require('../App/config/database.json');
			config_database = config_database[mode];
			if (config_database) {
				var database = require('./database');
				database(function (err , db){
					this.db = db;
					setup = true;
					if (err) {
						status_db = false;
						err_db = err;
					} else {
						status_db = true;
					}
					return cb(err , db);
				});
			} else {
				setup = false;
				status_db = false;
				return cb('Database Config Not Found', null)
			}
		} else {
			status_db = false;
			return cb('Database Config Not Found', null)
		}
	}

	this.initDatabase = function() {
		app.use(this.database);
	}

	this.database = function(req, res, next) {
		var driver = require('./driver/' + config_database.driver);
		req['db'] = new driver(this.db);
		next();
	}

	this.setLogger = (req, res, next) => {
		const winston = require('winston');
		const logger = winston.createLogger({
			level: 'info',
			format: winston.format.json(),
			transports: [
				new winston.transports.File({ filename: './log/error.log', level: 'error' }),
				new winston.transports.File({ filename: './log/info.log' })
			]
		});

		logger.add(new winston.transports.Console({
			format: winston.format.simple()
		}));

		req['logger'] = logger;
		next();
	}

	// config session , passport , redis 
	this.initSession = function () {
		if (config_session.redis != undefined ) {
			var	redisStore = require('connect-redis')(session);
			var redis = require('redis').createClient();
			app.use(session({
				secret : config_session.session.secret,
				store : new redisStore({
					host : config_session.redis.host.toString(),
					port : config_session.redis.port,
					saveUninitialized : config_session.session.saveUninitialized,
					resave : config_session.session.resave,
					client : redis,
					db : config_session.redis.db
				}),
				resave : config_session.session.resave
			}));
		} else if (config_session.mongodb != undefined) {
			var MongoStore = require('connect-mongo/es5')(session);
			config_database = require('../App/config/database.json');
			config_database = config_database[mode];
			var url = "";
			var db_session = config_database.mongodb.connection.db;
			if ( (config_database.mongodb.connection.user == '' || config_database.mongodb.connection.user == undefined ) && (config_database.mongodb.connection.pwd == '' || config_database.mongodb.connection.pwd == undefined )) {
				url = 'mongodb://'+config_database.mongodb.connection.host+':' + config_database.mongodb.connection.port + '/'+db_session;
			} else {
				url = 'mongodb://'+config_database.mongodb.connection.user+':'+config_database.mongodb.connection.pwd+'@'+config_database.mongodb.connection.host+':' + config_database.mongodb.connection.port + '/'+db_session;
			}
			app.use(session({
				secret: config_session.session.secret,
				saveUninitialized : config_session.session.saveUninitialized,
				resave : config_session.session.resave,
				store: new MongoStore({
					url: url
				})
			}));
		} else {
			app.use(session({
				secret : config_session.session.secret,
				saveUninitialized : config_session.session.saveUninitialized,
				resave : config_session.session.resave
			}));
		}

		if (setup == true) {
			if (config_session.passport != undefined ) {
				if (config_session.passport.auth == true ) {
					var authPassport = require('../App/plugins/passport');
					var initPassport = new authPassport();
					var passport = initPassport.getPassport();
					app.use(passport.initialize());
					app.use(passport.session());
					initPassport.passportInit();
				}
			}
		}
	}

	this.initAutoLoad = function() {
		if (setup == true) {
			app.use(this.autoload);
		}
	}

	this.initAvocado = function () {
		app.use(bodyParser.json());
		app.use(bodyParser.urlencoded({ extended: false }));
		app.use(xmlparser());
		if (configApp.app.logger == true) {
			app.use(this.setLogger);
		}
		// ## SETTING VIEW ENGINE & CSS PRECOMPILER
		var target = path.normalize(__dirname+'/..');
		app.set('views', path.join(target, './App/views'));
		app.set('view engine', 'jade');
		app.use(require('stylus').middleware(path.join(target, './App/public')));
		app.use(express.static(path.join(target, './App/public')));
		app.use(function (req, res, next) {
			res.header("Access-Control-Allow-Origin", "*");
			res.header("Access-Control-Allow-Headers", "X-Requested-With");
			res.header("Access-Control-Allow-Credentials", "true");
			// res.header("Access-Control-Allow-Methods : POST,PUT");
			next();
		});
		app.use(cookieParser('singleserpolmiddle'));
			if (status_db) {
				routes(app);
			} else {
				app.all('*', function (req , res , next) {
					return res.render('mongodError');
				});
			}
	}

	this.autoload = function(req , res , next ) {
		var plugins = autoload.plugins || null;
		var features = autoload.features || null;
		req['plugin'] = {};
		if (plugins != null) {
			plugins.forEach(function (plugin) {
				var module = require('../App/plugins/' + plugin);
				req['plugin'][plugin] = new module;
			});
		}

		if (features != null) {
			features.forEach(function (feature) {
				var module = require('../App/features/' + feature);
				req[feature] = new module;
			});
		}
		next();
	}
}

module.exports = Avocado;