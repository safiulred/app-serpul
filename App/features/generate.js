const bcrypt = require('bcrypt-nodejs');
const moment = require('moment');
moment.locale('id');
const request = require('request');
const shortid = require('shortid');
const _ = require('underscore');

function Generate() {

	this.enkripsi = (str) => {
		let arrText = new Array(" ","a","A","b","B","c","C","d","D","e","E","f","F","g","G","h","H","i","I","j","J","k","K","l","L","m","M","n","N","o","O","p","P","q","Q","r","R","s","S","t","T","u","U","v","V","w","W","x","X","y","Y","z","Z","`","1","2","3","4","5","6","7","8","9","0","~","!","@","$","^","*","(",")","-","{","}","|","[","]",":",";","<",">","?",",",".","/","_","=");

		let arr = str.split("");
		let jml = arrText.length;
		let jmlDouble = jml*2;
		let jmlTriple = jml*3;
		arrHasil = new Array();
		for (let i = 0; i < arr.length; i++){
			let xx = arrText.indexOf(arr[i]);
			let acak = xx+i;
			if (acak<jml){
				arrHasil[i]= arrText[acak];
			} else if ((acak >= jml) && (acak < jmlDouble)){
				arrHasil[i] = arrText[acak-jml];
			} else if ((acak >= jmlDouble) && (acak < jmlTriple)){
				arrHasil[i] = arrText[acak-jmlDouble];
			} else if (acak >= jmlTriple){
				arrHasil[i] = arrText[acak-jmlTriple];
			}
		}
		return arrHasil.join("");
	}

	this.GetId = function (string) {
		var character = string || '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$@';
		shortid.characters(character);
		return shortid.generate();
	}

	this.SetTanggal = function (tanggal , format) {
		if (tanggal == null) {
			return ''
		} else {
			var now = moment(tanggal).add(7, 'hours').format(format);
			return now;
		}
	}

	this.Tanggal = function () {
		var now = new Date(); 
		var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
		return  now_utc;
	}

	this.password = function (password) {
		var salt = bcrypt.genSaltSync();
		var password = bcrypt.hashSync(password,salt);
		return password;
	}

	this.elementOption = function (arr , options) {
		var listoptions = '';
		function initlistOption() {
			listoptions += '<select class = "form-control">';
			listoptions += '<option value = "" > -- Choose One --</option>';
			if (Array.isArray(arr) == true ) {
				for (var i = 0; i < arr.length; i++) {
					listoptions += '<option value = '+arr[i][options.name]+'>'+arr[i][options.name]+'</option>';
				}
			} 
			listoptions += '</select>'
		}

		initlistOption();
		if (options.tombol == true ) {
			listoptions += '<span id = "add-list" style="cursor:pointer;" class = "input-group-addon" ><i id = '+options.id_arg+' class = "fa fa-plus"></i></span>'
		}
		return listoptions;
	}

	this.elementList = function (arr , options) {
		var listdata = '';
		function initlist() {
			listdata += '<ul class = "list-group '+ options.kelas+'">';
			if (Array.isArray(arr) == true ) {
				for (var i = 0; i < arr.length; i++) {
					listdata += '<li id = '+arr[i][options.name]+' class = "list-group-item">'+ arr[i][options.name]+'<a style = "cursor : pointer;" class = "pull-right remove_list"><i class = "fa fa-trash-o"></i></a></li>'
				}
			}
			listdata += '</ul>'
		}
		initlist()
		return listdata;
	}

	this.GetVisible = function (obj , callback) {
		var type_visible = [];
		if (Array.isArray(obj.itemObject) == true ) {
			obj.itemObject.forEach(function (i){
				if (i.visible == true) {
					type_visible.push({
						label : i.label,
						type : i.type
					});
				}
			});
			return callback(null , type_visible);
		} else {
			return callback('not found' , null);
		}
	}
}
module.exports = Generate;