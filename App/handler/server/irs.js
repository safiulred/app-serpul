function IRS () {
	const querystring = require('query-string');
	const md5 = require('md5');
	const _ = require('underscore');
	const request = require('request');
	const fs = require('fs');
	const path = require('path');
	const mode = process.env.NODE_ENV || 'dev';
	var configApp = require('../../config/app.json')[mode] || require('../App/config/app.json')['dev'];
	
	// const ApiUrl = 'https://192.168.0.3:7227/singlePAY';
	// const ApiUrl = 'https://202.152.12.106:7227/singlePAY';
	const ApiUrl = configApp.app.ApiUrl; //'https://192.168.0.10:8443/singlePAY';

	// GET MESSAGE SUKSES INQ/PAY
	const getMsgReq = async (jenis) => {
		return new Promise(async (resolve, reject) => {
			let msg = "";
			switch (jenis) {
				case "5" :
					msg = "CEK TAGIHAN";
				break;
				case "6" :
					msg = "PEMBAYARAN TAGIHAN";
				break;
			}
			return resolve(msg);
		});
	}
	// END

	this.isAuth = async (req, res, next) => {
		let b = req.query;
		const url = req.url;
		req.logger.log('info', url);
		req.logger.log('info', b);
		if (b.pin == undefined || b.pin == "" ) {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, DATA PIN TIDAK DI KENALI / KOSONG  PIN:'+b.pin);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, DATA PIN TIDAK DI KENALI / KOSONG  PIN:'+b.pin);
		}

		if (b.tujuan == undefined || b.tujuan == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, DATA TUJUAN TIDAK DI KENALI / KOSONG  IDPEL:'+b.tujuan);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, DATA TUJUAN TIDAK DI KENALI / KOSONG  IDPEL:'+b.tujuan);
		}

		if (b.pass == undefined || b.pass == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, PASSWORD TIDAK DI KENALI / KOSONG  PASS:'+b.pass);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, PASSWORD TIDAK DI KENALI / KOSONG  PASS:'+b.pass);
		}

		if (b.kodeproduk == undefined || b.kodeproduk == "") {
			req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, KODE PRODUK TIDAK DI KENALI / KOSONG  KODEPRODUK:'+b.kodeproduk);
			req.logger.log('error', url);
			return res.send('ERRCODE:404 REQUEST DITOLAK, KODE PRODUK TIDAK DI KENALI / KOSONG  KODEPRODUK:'+b.kodeproduk);
		}

		// if (b.jenis == undefined || b.jenis == "") {
		// 	req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, JENIS REQUEST TIDAK DI KENALI / KOSONG  JENIS:'+b.jenis);
		// 	req.logger.log('error', url);
		// 	return res.send('ERRCODE:404 REQUEST DITOLAK, JENIS REQUEST TIDAK DI KENALI / KOSONG  JENIS:'+b.jenis);
		// }

		let password = b.pass;
		let username = b.pin;
		let tujuan = b.tujuan;
		req.db.findOne('jsessionid', { _id : username }, (err, docs) => {
			if (docs == null) {
				console.log('login ke getway singlePAY');
				let tmpUser = username+'~9e0d031d6908a681~1.0.0~andro';
				console.log(tmpUser);
				tmpUser = req.generate.enkripsi(tmpUser);
				let tmpPswd = md5(password);
				let tmpSogok = md5(tmpUser+"SUBHANALLAH-ALHAMDULILLAH");
				// let URL = ApiUrl+"/loginserpul?act=login&usr="+tmpUser+"&psw="+tmpPswd+"&sogok="+tmpSogok;
				let URL = ApiUrl+"/logingatewayserpul?act=login&usr="+tmpUser+"&psw="+tmpPswd+"&sogok="+tmpSogok;
				let options = {
					url: URL,
					method : 'GET',
				};
				console.log('URL : '+ URL);
				req.logger.log('info','login ke getway singlePAY');
				request(options, (err, response, body) => {
					if (!err && response.statusCode == 200) {
						if (body) {
							let info = JSON.parse(body);
							req.logger.log('info', info);
							console.log(info);
							if (info.rc == "00") {
								let setcookie = response.headers["set-cookie"][0];
								let params = querystring.parse(setcookie);
								let JSESSIONID = params.JSESSIONID;
								let pecah = JSESSIONID.split(';');
								let session = pecah[0];
								let dataInsert = {
									_id : username,
									password : password,
									JSESSIONID : session,
								}
								req.logger.log('info', 'request login sukses tujuan : '+tujuan+' JSESSIONID : '+ session+" untuk kodeproduk : "+b.kodeproduk);
								req.db.SaveData('jsessionid', dataInsert, (err, rows) => {
									req['JSESSIONID'] = session;
									return next();
								});
							} else {
								req.logger.log('error', 'request login gagal untuk  tujuan:'+tujuan);
								req.logger.log('error', info.data);
								return res.send(info.data+' IDPEL:'+tujuan+'@RC:'+info.rc);
							}
						} else {
							req.logger.log('error', 'request login gagal untuk tujuan:'+tujuan+" kodeproduk:"+b.kodeproduk);
							req.logger.log('error', 'response dari getway kosong');
							return res.send('ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG ');
						}
					} else {
						req.logger.log('error', 'request session gagal untuk tujuan:'+tujuan);
						req.logger.log('error', err);
						return res.send("ERRCODE:000 REQUEST SESSION GAGAL, SILAHKAN DIULANGI  tujuan:"+tujuan);
					}
				});
			} else {
				console.log('JSESSIONID SUDAH ADA DI DATABASE');
				req['JSESSIONID'] = docs.JSESSIONID;
				return next();
			}
		});
	}

	this.index = async (req, res, next) => {
		let b = req.query;
		let kodeproduk = b.kodeproduk;
		kodeproduk = kodeproduk.toLowerCase();
		let tujuan = b.tujuan;
		let idtrx = b.idtrx;
		let jenis = b.jenis;
		let counter = b.counter;
		let user = b.user;
		let pin = b.pin;
		let pass = b.pass;
		let id = b.id;
		const JSESSIONID = req.JSESSIONID;
		if (JSESSIONID == undefined) {
			req.logger.log('error', "SESSION SUDAH USANG tujuan:"+tujuan);
			return res.send('ERRCODE:403  SESSION TIDAK DI KENALI, SILAHKAN LAKUKAN INQUERY  tujuan:'+tujuan);
		} else {
			let arr_kode = ['pln','nontaglis','cekprepaid'];
			let cek = arr_kode.indexOf(kodeproduk);
			let cek_kode = kodeproduk.search('plnpre');
			console.log("cek_kode ====> "+ cek_kode);
			if (cek_kode == 0) {
				let options = await getOptions(req, JSESSIONID , {
					jenis : "5",
					kodeproduk : 'cekprepaid',
					kode : kodeproduk,
					tujuan : tujuan
				});
				reqData(options)
					.then(async (info) => {
						if (info.rc == "00") {
							let parseData = info.data.split('|');
							let nometer = parseData[0];
							let optionsPay = await getOptions(req, JSESSIONID, {
								kodeproduk : 'prepaid', 
								tujuan : nometer,
								kode : kodeproduk,
								jenis : "6"
							});
							request(optionsPay, async (err, response, body) => {
								if (!err && response.statusCode == 200) {
									if (body) {
										let infoPay = JSON.parse(body);
										if (infoPay.rc == "00") {
											let msgValue = await pilahData("6" ,'prepaid', infoPay.data);
											return res.send("PEMBELIAN LISTRIK PREPAID SUKSES "+msgValue);
										} else {
											let message = "";
											let rc = info.rc;
											if (infoPay.data == "null" || infoPay.data == "") {
												req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA tujuan:"+tujuan);
												req.logger.log('error', infoPay);
												message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
											} else {
												req.logger.log('error', infoPay.data+" tujuan:"+tujuan);
												message = infoPay.data;
											}
											return res.send(message+' IDPEL:'+b.tujuan+'@RC:'+rc+'@');
										}
									} else {
										req.logger.log('error', 'hasil request kosong gagal untuk pin: tujuan:'+tujuan);
										req.logger.log('error', 'response dari getway kosong');
										return res.send('ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG ');
									}
								} else {
									req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan)
									req.logger.log('error', err);
									return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan);
								}
							});
						} else if (info.rc == "999") {
							let options ={
								jenis : "5",
								kodeproduk : 'cekprepaid',
								kode : kodeproduk,
								tujuan : tujuan,
								flag : true,
							}
							rollback(req, options, async (err, resOtpions) => {
								if (err) return res.send(err);
								let SESSIONID = resOtpions.JSESSIONID;
								reqData(resOtpions.options)
									.then(async (output) => {
										if (output.rc == "00") {
											let parseData = output.data.split('|');
											let nometer = parseData[0];
											let optionsPay = await getOptions(req, SESSIONID, {
												kodeproduk : 'prepaid', 
												tujuan : nometer,
												kode : kodeproduk,
												jenis : "6"
											});
											request(optionsPay, async (err, response, body) => {
												if (!err && response.statusCode == 200) {
													if (body) {
														let infoPay = JSON.parse(body);
														if (infoPay.rc == "00") {
															let msgValue = await pilahData("6" ,'prepaid', infoPay.data);
															return res.send("PEMBELIAN LISTRIK PREPAID SUKSES "+msgValue);
														} else {
															let message = "";
															let rc = info.rc;
															if (infoPay.data == "null" || infoPay.data == "") {
																req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA tujuan:"+tujuan);
																req.logger.log('error', infoPay);
																message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
															} else {
																req.logger.log('error', infoPay.data+" tujuan:"+tujuan);
																message = infoPay.data;
															}
															return res.send(message+' IDPEL:'+tujuan+'@RC:'+rc+'@');
														}
													} else {
														req.logger.log('error', 'hasil request kosong gagal untuk pin: tujuan:'+tujuan);
														req.logger.log('error', 'response dari getway kosong');
														return res.send('ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG ');
													}
												} else {
													req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan)
													req.logger.log('error', err);
													return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan);
												}
											});
										} else {
											req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan)
											return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN");
										}
									})
									.catch(async (err) => {
										console.log('ERROR 1', err);
										req.logger.log('error', "ERRCODE:001 REQUEST ULANG GAGAL, TERJADI KESALAHAN tujuan:"+tujuan)
										req.logger.log('error', err);
										return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan);
									})
							});
						} else {
							let message = "";
							let rc = info.rc;
							if (info.data == "null" || info.data == "") {
								req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA tujuan:"+tujuan);
								req.logger.log('error', info);
								message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
							} else {
								req.logger.log('error', info.data+" tujuan:"+tujuan);
								message = info.data;
							}
							return res.send(message+' IDPEL:'+b.tujuan+'@RC:'+rc+'@');
						}
					})
					.catch(async (err) => {
						console.log('ERROR 1', err);
						req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan)
						return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN  tujuan:"+tujuan);
					});
			} else if (cek != -1) {
				if (b.jenis == undefined || b.jenis == "") {
					const url = req.url;
					req.logger.log('error', 'ERRCODE:404 REQUEST DITOLAK, JENIS REQUEST TIDAK DI KENALI / KOSONG  JENIS:'+b.jenis);
					req.logger.log('error', url);
					return res.send('ERRCODE:404 REQUEST DITOLAK, JENIS REQUEST TIDAK DI KENALI / KOSONG  JENIS:'+b.jenis);
				}
				let options = await getOptions(req, JSESSIONID, b); // get options paramater
				let msgReq = await getMsgReq(jenis);
				reqData(options)
					.then(async (info) => {
						console.log('hasil request pertama');
						console.log(info);
						if (info.rc == "00") {
							req.logger.log('info', "request sukses tujuan:"+tujuan);
							let msgValue = await pilahData(jenis ,kodeproduk, info.data);
							kodeproduk = kodeproduk.replace('cekprepaid',"prepaid");
							return res.send(msgReq+" "+kodeproduk.toUpperCase()+" SUKSES "+msgValue);
						} else if (info.rc == "999") {
							req.logger.log('error', msgReq+" "+b.kodeproduk.toUpperCase()+" GAGAL, tujuan:"+tujuan);
							req.logger.log('error', info.data);
							// if (jenis == "5") {
							rollback(req, b, async (err, resOtpions) => {
								if (err) return res.send(err);
								return res.json(resOtpions);
								reqData(resOtpions)
									.then(async (output) => {
										if (output.rc == "00") {
											req.logger.log('info', "request ulang sukses tujuan:"+tujuan);
											let valueMSG = await pilahData(jenis ,kodeproduk, output.data);
											kodeproduk = kodeproduk.replace('cekprepaid',"prepaid");
											return res.send(msgReq+" "+kodeproduk.toUpperCase()+" SUKSES "+valueMSG);
										} else {
											let message = "";
											let rc = output.rc;
											if (output.data == "null" || output.data == "") {
												req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA tujuan:"+tujuan);
												req.logger.log('error', output);
												message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
											} else {
												req.logger.log('error', output.data+" tujuan:"+tujuan);
												message = output.data;
											}
											return res.send(message+' IDPEL:'+b.tujuan+'@RC:'+rc+'@');
										}
									})
									.catch(async (err) => {
										req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan)
										return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN");
									});
							})
							// } else {
							// 	req.logger.log('error', "SESSION SUDAH USANG tujuan:"+tujuan);
							// 	return res.send(' ERRCODE:403  SESSION TIDAK DI KENALI, SILAHKAN LAKUKAN INQUERY');
							// }
						} else {
							let message = "";
							let rc = info.rc;
							if (info.data == "null" || info.data == "") {
								req.logger.log('error', "ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA tujuan:"+tujuan);
								req.logger.log('error', info);
								message = " ERRCODE:404 NOMOR METER/IDPEL BELUM TERSEDIA";
							} else {
								req.logger.log('error', info.data+" tujuan:"+tujuan);
								message = info.data;
							}
							return res.send(message+' IDPEL:'+b.tujuan+'@RC:'+rc+'@');
						}
					})
					.catch(async (err) => {
						console.log('ERROR 1', err);
						req.logger.log('error', "ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN tujuan:"+tujuan)
						return res.send("ERRCODE:001 REQUEST GAGAL, TERJADI KESALAHAN  tujuan:"+tujuan);
					});
			} else {
				req.logger.log('error', "PRODUCT BELUM TERSEDIA  kodeproduk:"+kodeproduk);
				return res.send('ERRCODE:404 PRODUCT BELUM TERSEDIA  kodeproduk:'+kodeproduk);
			}
		}
	}

	const rollback = async (req, params, callback) => {
		let tujuan = params.tujuan;
		let password = params.pass;
		let username = params.pin;
		let tmpUserLogin = req.generate.enkripsi(username+'~9e0d031d6908a681~1.0.0~andro');
		let tmpPswdLogin = md5(password);
		let tmpSogokLogin = md5(tmpUserLogin+"SUBHANALLAH-ALHAMDULILLAH");
		let URLlogin = ApiUrl+"/logingatewayserpul?act=login&usr="+tmpUserLogin+"&psw="+tmpPswdLogin+"&sogok="+tmpSogokLogin;
		let optionsLogin = {
			url: URLlogin,
			method : 'GET',
		}
		console.log(URLlogin);
		console.log('login ulang');
		request(optionsLogin, async (err, response, body) => {
			if (!err && response.statusCode == 200) {
				if (body) {
					let infoLogin = JSON.parse(body);
					console.log('hasil dari rollback');
					console.log(infoLogin);
					if (infoLogin.rc == "00") {
						let setcookie = response.headers["set-cookie"][0];
						let query = querystring.parse(setcookie);
						let JSESSIONID = query.JSESSIONID;
						let pecah = JSESSIONID.split(';');
						let session = pecah[0];
						console.log('get session ulang ===> '+ session);
						req.logger.log('info', 'request login ulang sukses  tujuan : '+tujuan+' JSESSIONID : '+ session+" untuk product : "+params.kodeproduk);
						req.db.update('jsessionid', {_id : username }, { $set : { JSESSIONID : session }} , (err, rows) => {
						});
						let optionsRequest = await getOptions(req, session, params);
						if (params.flag) {
							return callback(null ,{ options : optionsRequest, JSESSIONID : session});
						} else {
							return callback(null ,optionsRequest);
						}
					} else {
						req.logger.log('error', 'request login ulang gagal');
						req.logger.log('error', infoLogin.data+" tujuan:"+tujuan+" password:"+params.password);
						return callback(infoLogin.data+" tujuan:"+tujuan , null);
					}
				} else {
					req.logger.log('error', 'request login gagal untuk tujuan:'+tujuan+" kodeproduk:"+b.kodeproduk);
					req.logger.log('error', 'response dari getway kosong');
					return res.send('ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG ');
				}
			} else {
				req.logger.log('error', 'request login ulang gagal tujuan:'+tujuan);
				req.logger.log('error', err);
				return callback('ERRCODE:000 REQUEST SESSION GAGAL, SILAHKAN DIULANGI tujuan:'+tujuan, null);
			}
		});
	}

	const pilahData = async (jenis , kodeproduk, responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			switch(kodeproduk.toLowerCase()) {
				case "pln" :
					if (jenis == "5") {
						hasil = await inqPost(responseData);
					} else if (jenis == "6") {
						hasil = await payPost(responseData);
					}
				break;
				case "prepaid" :
					hasil = await payPre(responseData);
				break;
				case "cekprepaid" :
					hasil = await inqPre(responseData);
				break;
				case "nontaglis" :
					if (jenis == "5") {
						hasil = await inqNontaglis(responseData);
					} else if (jenis == "6") {
						hasil = await payNontaglis(responseData);
					}
				break;
			}
			return resolve(hasil);
		});
	}

	const inqPre = async (responseData) => {
		return new Promise (async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			hasil +="METER : "+ arr[0]+"@";
			hasil +="IDPEL : "+ arr[1]+"@";
			hasil +="NAMA : "+ arr[2]+"@";
			hasil +="TARIF/DAYA : "+ arr[4]+"@";
			hasil +="RP ADM : "+ arr[7]+"@";
			return resolve(hasil);
		})
	}

	const inqNontaglis = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			hasil +="IDPEL : "+ arr[0]+"@";
			hasil +="NAMA : "+ arr[1]+"@";
			hasil +="RP TAG : "+ arr[4]+"@";
			hasil +="ADM : "+ arr[3]+"@";
			hasil +="TOTAL TAGIHAN : "+ arr[7]+"@";
			return resolve(hasil);
		});
	}

	const inqPost = async (responseData) => {
		return new Promise(async (resolve, reject) => {
			let hasil = "";
			let data = responseData.split('|');
			let rptag = data[7] - data[4];
			hasil += "IDPEL : "+data[0];
			hasil += "@NAMA : "+data[1];
			hasil += "@BLN : "+data[2];
			hasil += "@JML : "+data[3];
			hasil += "@RP TAG : "+rptag;
			hasil += "@ADM : "+data[4];
			hasil += "@SM : "+data[5];
			hasil += "@TOTAL TAGIHAN : "+data[7]+"@"
			return resolve(hasil);
		});
	}

	const payNontaglis = async (responseData) => {
		return new Promise (async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			let total_tagihan = Number(arr[16]) + Number(arr[17]);
			hasil += "NOREG : "+arr[2]+"@";
			hasil += "IDPEL : "+arr[3]+"@";
			hasil += "NAMA : "+arr[5]+"@";
			hasil += "TGL REG : "+arr[6]+"@";
			hasil += "LAYANAN : "+arr[24]+"@";
			hasil += "INFO : "+arr[25]+"@";
			hasil += "TLP UNIT PLN : "+arr[26]+"@";
			hasil += "NOREF : "+arr[14]+"@";
			hasil += "HARGA : "+arr[16]+"@";
			hasil += "RP ADM : "+arr[17]+"@";
			hasil += "TOTAL TAGIHAN : "+total_tagihan+"@";
			hasil += "SALDO : "+arr[28]+"@";
			return resolve(hasil);
			// hasil += "TGL BAYAR : "+arr[15]+"@";
		});
	}

	const payPost = async (responseData) => {
		return new Promise (async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			let total_tagihan = Number(arr[16]) + Number(arr[17]);
			hasil += "IDPEL : "+arr[2]+"@";
			hasil += "NAMA : "+arr[5]+"@";
			hasil += "BLTH : "+arr[3]+"@";
			hasil += "TARIF : "+arr[6]+"@";
			hasil += "DAYA : "+arr[7]+"@";
			// hasil += "LAYANAN : "+arr[24]+"@";
			// hasil += "TGL BAYAR : "+arr[15]+"@";
			hasil += "INFO : "+arr[25]+"@";
			hasil += "TLP UNIT PLN : "+arr[26]+"@";
			hasil += "NOREF : "+arr[14]+"@";
			hasil += "RP TAG : "+arr[16]+"@";
			hasil += "RP ADM : "+arr[17]+"@";
			hasil += "TOTAL TAGIHAN : "+total_tagihan+"@";
			hasil += "SALDO : "+arr[28]+"@";
			return resolve(hasil);
		});
	}

	const payPre = async (responseData) => {
		return new Promise (async (resolve, reject) => {
			let hasil = "";
			let arr = responseData.split('|');
			// let total_tagihan = Number(arr[16]) + Number(arr[17]);
			let rptag = Number(arr[16]) - Number(arr[17]);
			let token = arr[14];
			let newToken = token.toString().match(/.{4}/g).join('-');

			hasil +="NOMETER :"+arr[2]+"@";
			hasil +="IDPEL :"+arr[8]+"@";
			hasil +="NAMA :"+arr[5]+"@";
			hasil +="TARIF :"+arr[6]+"@";
			hasil +="DAYA :"+arr[7]+"@";
			hasil +="TOKEN :"+newToken+"@";
			// hasil +="TOKEN :"+arr[14]+"@";
			hasil +="JML KWH :"+arr[18].replace(/#/g,",")+"@";
			hasil +="RP TOKEN :"+arr[23].replace(/#/g,",")+"@";
			hasil +="PJU :"+arr[19].replace(/#/g,",")+"@";
			hasil +="ANGSURAN :"+arr[20].replace(/#/g,",")+"@";
			hasil +="MATERAI :"+arr[3].replace(/#/g,",")+"@";
			hasil +="PPN :"+arr[4].replace(/#/g,",")+"@";
			hasil +="NOREF :"+arr[22]+"@";
			hasil +="HARGA :"+rptag+"@";
			hasil +="RP ADM :"+arr[17]+"@";
			hasil += "TOTAL TAGIHAN : "+arr[16]+"@";
			hasil +="SALDO : "+arr[28]+"@";
			return resolve(hasil);
		});
	}

	const reqData = async (options) => {
		return new Promise(async ( resolve, reject) => {
			try {
				request(options, (err, response, body) => {
					if (!err && response.statusCode == 200) {
						if (body) {
							let info = JSON.parse(body);
							return resolve(info);
						} else {
							return reject("ERRCODE:203 REQUEST GAGAL, DATA KONTEN KOSONG");
						}
					} else {
						return reject(err);
					}
				});
			} catch (err) {
				return reject(err);
			}
		});
	}

	const getOptions = async (req ,JSESSIONID , params) => {
		return new Promise(async (resolve, reject) => {
			let jenis = params.jenis;
			let tmpIsi = "";
			let jnsReq = await getJnsRequest(params.jenis);
			let jnsLayanan = await getProduk(params.kodeproduk);
			let ArrayID = await getArrayID (params, jnsLayanan);
			tmpIsi = jnsReq+'~'+jnsLayanan+'~'+ArrayID+'~'+JSESSIONID;
			console.log('tmpIsi ==> '+ tmpIsi);
			tmpIsi = req.generate.enkripsi(tmpIsi);
			let URL = ApiUrl+'/requestserpul?isi='+tmpIsi+'&sogok='+JSESSIONID;
			let cookie = "JSESSIONID="+JSESSIONID+";";
			let options = {
				url : URL,
				method : 'GET',
				headers: {
					'Cookie' : cookie,
				}
			}
			return resolve(options);
		});
	}

	// FILTER DATA JENIS LAYANAN
	const getProduk = async (kodeproduk) => {
		return new Promise(async (resolve, reject) => {
			kodeproduk = kodeproduk.toLowerCase();
			let jnsLayanan = "";
			switch(kodeproduk) {
				case "pln" :
					jnsLayanan = "post";
				break;
				case "nontaglis" :
					jnsLayanan = "nebill";
				break;
				case "cekprepaid" :
					jnsLayanan = "pre";
				break;
				case "prepaid" :
					jnsLayanan = "prepaid";
				break;
			}
			return resolve(jnsLayanan);
		});
	}
	// END

	// FILTER DATA JENIS REQUEST
	const getJnsRequest = async (jenis) => {
		return new Promise(async (resolve, reject) => {
			let jnsReq = "";
			switch(jenis) {
				case "5" :
					jnsReq = "inq";
				break;
				case "6" :
					jnsReq = "pay";
				break;
			}
			return resolve(jnsReq);
		});
	}
	// END

	// GET ARRAYID
	const getArrayID = async (params, jnsLayanan) => {
		return new Promise(async (resolve, reject) => {
			let tujuan = params.tujuan;
			let kode = params.kode;
			let ArrayID = "";
			switch(jnsLayanan) {
				case "post" : 
					ArrayID = tujuan+'!0!0';
				break;
				case "nebill" :
					ArrayID = tujuan;
				break;
				case "pre" :
					ArrayID = tujuan+'!0';
				break;
				case "prepaid" :
					let denom = kode.replace('plnpre!','');
					denom +="000";
					console.log("denom ===> "+ denom);
					ArrayID = tujuan+'!'+denom+'!BARU!0';
				break;
			}
			return resolve(ArrayID);
		});
	}
	// END
}

module.exports =  IRS;